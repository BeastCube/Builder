package net.beastcube.builder;

import net.beastcube.api.bukkit.config.annotation.AnnotationConfiguration;
import net.beastcube.api.bukkit.config.annotation.Setting;
import net.beastcube.api.bukkit.config.annotation.SettingBase;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author BeastCube
 */
@SettingBase("builder")
public class BuilderConfig extends AnnotationConfiguration {
    @Setting
    public String exportWorldsFolder = "";

    private Plugin plugin;
    private static FileConfiguration config;

    public BuilderConfig(JavaPlugin plugin) {
        this.plugin = plugin;
        config = plugin.getConfig();
        this.load();
        this.save();
    }

    public void load() {
        load(config);
    }

    public void save() {
        save(config);
        plugin.saveConfig();
    }

}
