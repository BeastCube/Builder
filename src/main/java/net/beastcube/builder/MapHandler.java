package net.beastcube.builder;

import net.beastcube.api.commons.minigames.MinigameType;
import net.beastcube.minigameapi.Minigame;
import net.beastcube.minigameapi.MinigameAPI;
import net.beastcube.minigameapi.arena.Arena;
import net.minecraft.server.v1_8_R3.ExceptionWorldConflict;
import net.minecraft.server.v1_8_R3.IProgressUpdate;
import net.minecraft.server.v1_8_R3.WorldServer;
import org.bukkit.*;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author BeastCube
 */
public class MapHandler implements Listener {

    private List<BuilderMap> openMaps;
    private Builder plugin;

    public MapHandler(Builder plugin) {
        openMaps = new ArrayList<>();
        this.plugin = plugin;
    }

    public Optional<BuilderMap> getBuilderMap(MinigameType minigame, String name) {
        return openMaps.stream().filter(m -> m.getName().equals(name) && m.getMinigame() == minigame).findFirst();
    }

    public Optional<BuilderMap> getBuilderMap(World world) {
        return openMaps.stream().filter(m -> m.getWorld() == world).findFirst();
    }

    public boolean doesMapExist(MinigameType minigame, String name) {
        if (getBuilderMap(minigame, name).isPresent()) {
            return true;
        }

        File maps = new File(plugin.getDataFolder(), "/worlds/" + minigame.name());
        for (File dir : maps.listFiles()) {
            if (dir.isDirectory()) {
                if (dir.getName().equals(name)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * This method does overwrite existing maps!!!
     */
    @SuppressWarnings("ResultOfMethodCallIgnored")
    public boolean createNewMap(Player p, MinigameType minigame, String name) {
        try {
            Optional<Class<? extends JavaPlugin>> clazz = Builder.getInstance().getMinigame(minigame);
            if (clazz.isPresent()) {
                File dir = new File(plugin.getDataFolder(), "/worlds/" + clazz.get().getAnnotation(Minigame.class).minigameType().name() + "/" + name);
                if (dir.exists())
                    dir.delete();
                dir.mkdir();

                Class<? extends Arena> arenaClass = clazz.get().getAnnotation(Minigame.class).arena();
                Constructor constructor = arenaClass.getDeclaredConstructor();
                constructor.setAccessible(true);
                Arena arena = (Arena) constructor.newInstance();

                WorldCreator creator = new WorldCreator(name);
                creator.generator(clazz.get().getAnnotation(Minigame.class).worldGenerator() + ":", Bukkit.getConsoleSender()); //TODO make it better
                creator.type(WorldType.FLAT); //We want the void darkness and particles to start at around Y=0 instead of Y=64
                Builder.getInstance().getLogger().info(creator.generator().toString());
                World world = Bukkit.createWorld(creator);

                world.setSpawnLocation(0, 65, 0);

                BuilderMap bMap = new BuilderMap(name, world, arena, minigame);
                openMaps.add(bMap);
                p.sendMessage(ChatColor.GREEN + "Die Map wurde erfolgreich erstellt");
                return true;
            } else {
                p.sendMessage(ChatColor.RED + "Dieses Minigame existiert nicht. Hast du dich vertippt?");
                Builder.getInstance().listMinigames(p);
            }
        } catch (Exception e) {
            p.sendMessage(ChatColor.RED + "Beim Erstellen der Map ist ein Fehler aufgetreten: " + e.getMessage());
            e.printStackTrace();
        }
        return false;
    }

    public boolean deleteMap(Player p, MinigameType minigame, String name) {
        Optional<Class<? extends JavaPlugin>> clazz = Builder.getInstance().getMinigame(minigame);
        if (clazz.isPresent()) {
            File dir = new File(plugin.getDataFolder(), "/worlds/" + clazz.get().getAnnotation(Minigame.class).minigameType().name() + "/" + name);
            if (dir.exists()) {
                dir.delete();
                p.sendMessage(ChatColor.GREEN + "Die Map wurde erfolgreich gelöscht");
                return true;
            }
            p.sendMessage(ChatColor.RED + "Die Map existiert nicht");
        } else {
            p.sendMessage(ChatColor.RED + "Dieses Minigame existiert nicht. Hast du dich vertippt?");
            Builder.getInstance().listMinigames(p);
        }
        return false;
    }

    public boolean openMap(Player p, String name, MinigameType minigame) {
        Optional<Class<? extends JavaPlugin>> clazz = Builder.getInstance().getMinigame(minigame);
        if (clazz.isPresent()) {
            Optional<BuilderMap> map = getBuilderMap(minigame, name);
            if (!doesMapExist(minigame, name)) {
                p.sendMessage(ChatColor.RED + "Diese Map existiert nicht!");
                return false;
            } else if (map.isPresent()) {
                p.sendMessage(ChatColor.RED + "Diese Map wurde bereits geladen!");
                return false;
            }

            Arena arena;

            try {
                copyMapIntoBuffer(name, clazz.get());

                WorldCreator creator = new WorldCreator(name);
                creator.generator(clazz.get().getAnnotation(Minigame.class).worldGenerator() + ":", Bukkit.getConsoleSender()); //TODO make it better
                creator.type(WorldType.FLAT); //We want the void darkness and particles to start at around Y=0 instead of Y=64
                World world = Bukkit.createWorld(creator);
                world.setGameRuleValue("doMobSpawning", "false"); //Disable mob spawning by default

                Class<? extends Arena> arenaClass = clazz.get().getAnnotation(Minigame.class).arena();

                File file = new File(plugin.getDataFolder(), "/worlds/" + clazz.get().getAnnotation(Minigame.class).minigameType().toString() + "/" + name + "/arena.json");
                arena = MinigameAPI.loadArena(file, name, arenaClass);
                map = Optional.of(new BuilderMap(name, world, arena, minigame));

                openMaps.add(map.get());

                p.sendMessage(ChatColor.GREEN + "Die Map wurde erfolgreich geöffnet");
                return true;
            } catch (IOException | InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
                p.sendMessage(ChatColor.RED + "Beim Öffnen der Map ist ein Fehler aufgetreten");
            }
        } else {
            p.sendMessage(ChatColor.RED + "Dieses Minigame existiert nicht. Hast du dich vertippt?");
            Builder.getInstance().listMinigames(p);
        }
        return false;
    }

    public boolean closeMap(Player p, MinigameType minigame, String name) {
        Optional<BuilderMap> map = getBuilderMap(minigame, name);
        if (!doesMapExist(minigame, name)) {
            status(p, ChatColor.RED + "Diese Map existiert nicht!");
            return false;
        } else if (!map.isPresent()) {
            status(p, ChatColor.RED + "Diese Map wurde nicht geladen!");
            return false;
        }

        Bukkit.getOnlinePlayers().stream().filter(player -> player.getWorld() == map.get().getWorld()).forEach(player -> player.teleport(plugin.getLobbySpawn()));

        openMaps.remove(map.get());

        Bukkit.unloadWorld(map.get().getWorld(), false);

        File file = new File(Bukkit.getWorldContainer(), name);
        DirectoryHandler.deleteFolder(file);

        status(p, ChatColor.GREEN + "Die Map wurde erfolgreich geschlossen");
        return true;
    }

    private void saveWorldSync(World world) {
        try {
            Field worldServerField = CraftWorld.class.getDeclaredField("world");
            worldServerField.setAccessible(true);
            WorldServer worldServer = (WorldServer) worldServerField.get(world);

            boolean ex = worldServer.savingDisabled;
            worldServer.savingDisabled = false;
            worldServer.save(true, new IProgressUpdate() {
                @Override
                public void a(String s) {
                    Builder.getInstance().getLogger().info(s);
                }

                @Override
                public void c(String s) {
                    Builder.getInstance().getLogger().info(s);
                }

                private String[] status = {"Ooo", "oOo", "ooO", "oOo"};

                @Override
                public void a(int i) {
                    Builder.getInstance().getLogger().info("Saving world " + world.getName() + "  " + status[i % status.length]);
                }
            });
            worldServer.flushSave();
            worldServer.savingDisabled = ex;

        } catch (IllegalAccessException | ExceptionWorldConflict | NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    public boolean saveMap(Player p, MinigameType selectedMinigame, String name) {
        Optional<Class<? extends JavaPlugin>> clazz = Builder.getInstance().getMinigame(selectedMinigame);
        if (clazz.isPresent()) {
            Optional<BuilderMap> map = getBuilderMap(selectedMinigame, name);
            if (!doesMapExist(selectedMinigame, name)) {
                status(p, ChatColor.RED + "Diese Map existiert nicht!");
                return false;
            } else if (!map.isPresent()) {
                status(p, ChatColor.RED + "Diese Map wurde nicht geladen!");
                return false;
            }

            saveWorldSync(map.get().getWorld());

            try {
                map.get().saveArena();

                copyMapIntoArchive(map.get().getName(), clazz.get());

                map.get().setDirty(false);
                status(p, ChatColor.GREEN + "Die Map wurde erfolgreich gespeichert.");
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                status(p, ChatColor.RED + "Die Map konnte aufgrund eines Fehlers nicht richtig gespeichert werden.");
            }
        } else {
            status(p, ChatColor.RED + "Dieses Minigame existiert nicht. Hast du dich vertippt?");
            if (p != null) Builder.getInstance().listMinigames(p);
        }
        return false;
    }

    public void status(Player p, String status) {
        if (p != null) p.sendMessage(status);
    }

    public boolean exportMap(Player p, MinigameType selectedMinigame, String name) {
        Optional<Class<? extends JavaPlugin>> clazz = Builder.getInstance().getMinigame(selectedMinigame);
        if (clazz.isPresent()) {
            Optional<BuilderMap> map = getBuilderMap(selectedMinigame, name);
            if (!doesMapExist(selectedMinigame, name)) {
                status(p, ChatColor.RED + "Diese Map existiert nicht!");
                return false;
            } else if (!map.isPresent()) {
                status(p, ChatColor.RED + "Diese Map wurde nicht geladen!");
                return false;
            }

            String exportWorldsFolder = Builder.getInstance().getBuilderConfig().exportWorldsFolder;
            if (!exportWorldsFolder.equals("")) {
                exportWorldsFolder = exportWorldsFolder.endsWith(File.separator) ? exportWorldsFolder : exportWorldsFolder + File.separator;
                File dest = new File(exportWorldsFolder + selectedMinigame);
                if (!dest.exists()) {
                    if (!dest.mkdirs()) {
                        p.sendMessage("Fehler beim Erstellen des Ordners: " + dest.getAbsolutePath());
                        return false;
                    }
                }

                try {
                    File dir = new File(plugin.getDataFolder(), "/worlds/" + selectedMinigame + "/" + name);
                    DirectoryHandler.copyFolder(dir, new File(dest, name));
                    p.sendMessage(ChatColor.GREEN + "Die Map wurde erfolgreich exportiert.");
                    return true;
                } catch (IOException e) {
                    e.printStackTrace();
                    p.sendMessage(ChatColor.RED + "Die Map konnte aufgrund eines Fehlers nicht exportiert werden.");
                }
            } else {
                p.sendMessage(ChatColor.RED + "exportWorldsFolder ist leer. Bitte ändere dies in der Konfiguration!");
                return false;
            }
        } else {
            p.sendMessage(ChatColor.RED + "Dieses Minigame existiert nicht. Hast du dich vertippt?");
            Builder.getInstance().listMinigames(p);
        }
        return false;
    }

    public Optional<String[]> getAllMaps(Player p, MinigameType minigame) {
        Optional<Class<? extends JavaPlugin>> clazz = Builder.getInstance().getMinigame(minigame);
        if (clazz.isPresent()) {
            File dir = new File(plugin.getDataFolder(), "/worlds/" + minigame.toString());
            if (!dir.exists()) {
                Optional.empty();
            }

            return Optional.of(dir.list());
        } else {
            p.sendMessage(ChatColor.RED + "Dieses Minigame existiert nicht. Hast du dich vertippt?");
            Builder.getInstance().listMinigames(p);
        }
        return Optional.empty();
    }

    public Location getSpawn(MinigameType minigame, String name) {
        Optional<BuilderMap> map = getBuilderMap(minigame, name);
        if (map.isPresent()) {
            if (map.get().getArena() != null) {
                if (map.get().getArena().getSpectatorSpawn() != null)
                    return map.get().getArena().getSpectatorSpawn();
            }
            return map.get().getWorld().getSpawnLocation();
        }
        return null;
    }


    private void copyMapIntoBuffer(String map, Class<? extends JavaPlugin> clazz) {
        File dir = new File(plugin.getDataFolder(), "/worlds/" + clazz.getAnnotation(Minigame.class).minigameType().toString() + "/" + map);
        if (!dir.exists()) {
            return;
        }

        try {
            DirectoryHandler.copyFolder(dir, new File(Bukkit.getWorldContainer(), map));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void copyMapIntoArchive(String map, Class<? extends JavaPlugin> clazz) {
        File dir = new File(plugin.getDataFolder(), "/worlds/" + clazz.getAnnotation(Minigame.class).minigameType().toString() + "/" + map);
        if (!dir.exists()) {
            return;
        }

        try {
            DirectoryHandler.copyFolder(new File(Bukkit.getWorldContainer(), map), dir);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
