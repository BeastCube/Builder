package net.beastcube.builder.heads;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import net.beastcube.builder.Builder;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * @author Autom
 */
public class SkullUtils {

    public static JsonArray fromUrl(String urlString) {
        String all = readUrl(urlString);
        String body = all.substring(all.indexOf("<body>") + 6, all.indexOf("</body>")).trim();
        return new JsonParser().parse(body).getAsJsonArray();
    }

    private static String readUrl(String urlString) {
        HttpURLConnection request = null;
        try {
            URL url = new URL(urlString);
            request = (HttpURLConnection) url.openConnection();

            request.setDoOutput(true);
            request.setDoInput(true);
            request.setInstanceFollowRedirects(false);
            request.setRequestMethod("POST");
            request.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            request.setRequestProperty("charset", "UTF-8");
            request.setUseCaches(false);

            int responseCode = request.getResponseCode();

            if (responseCode == 200) {
                String encoding = request.getContentEncoding();
                encoding = encoding == null ? "UTF-8" : encoding;
                return IOUtils.toString(request.getInputStream(), encoding);
            }
        } catch (IOException e) {
            Builder.getInstance().getLogger().severe("URL not found");
        } finally {

            if (request != null) {
                request.disconnect();
            }
        }
        return null;
    }
}
