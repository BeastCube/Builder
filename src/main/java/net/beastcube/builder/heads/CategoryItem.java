package net.beastcube.builder.heads;

import lombok.Getter;

/**
 * @author BeastCube
 */
public enum CategoryItem {
    FOOD,
    DEVICES,
    MISC,
    ALPHABET,
    INTERIOR,
    COLOR,
    BLOCKS,
    MOBS,
    GAMES,
    CHARACTERS,
    POKEMON,
    XMAS(false);

    @Getter
    private boolean mainApi = true;

    public String getName() {
        return name().toLowerCase();
    }

    public String getUrl() {
        return "http://heads.freshcoal.com/" + (mainApi ? "main" : "") + "api.php?query=" + getName();
    }

    CategoryItem() {
    }

    CategoryItem(boolean mainApi) {
        this.mainApi = mainApi;
    }
}
