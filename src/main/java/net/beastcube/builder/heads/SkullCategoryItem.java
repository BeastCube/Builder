package net.beastcube.builder.heads;

import net.beastcube.api.bukkit.menus.events.ItemClickEvent;
import net.beastcube.api.bukkit.menus.items.*;
import net.beastcube.api.bukkit.menus.menus.ItemMenu;
import net.beastcube.api.bukkit.menus.menus.PagedMenu;
import net.beastcube.builder.Builder;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.List;

/**
 * @author BeastCube
 */
public class SkullCategoryItem extends MenuItem {


    private final List<ItemStack> skulls;
    private final String categoryName;
    private final ItemMenu parent;

    public SkullCategoryItem(String displayName, List<ItemStack> skulls, ItemMenu parent) {
        super(displayName, skulls.stream().findFirst().orElse(new ItemStack(Material.SKULL_ITEM)), "");
        this.categoryName = displayName;
        this.skulls = skulls;
        this.parent = parent;
    }

    @Override
    public void onItemClick(ItemClickEvent event) {
        PagedMenu menu = new PagedMenu(ChatColor.YELLOW + categoryName, ItemMenu.Size.SIX_LINE, Builder.getInstance(), parent);
        for (int i = 0; i < skulls.size(); i++) {
            SkullItem item = new SkullItem(skulls.get(i));
            menu.setItem(i, item);
        }
        menu.setMenuItem(0, new PreviousPageItem());
        menu.setMenuItem(8, new NextPageItem());
        menu.setMenuItem(4, new CurrentPageItem());
        menu.setMenuItem(5, new BackItem());
        menu.open(event.getPlayer());
    }

}
