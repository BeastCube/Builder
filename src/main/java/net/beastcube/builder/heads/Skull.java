package net.beastcube.builder.heads;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import net.minecraft.server.v1_8_R3.Items;
import net.minecraft.server.v1_8_R3.NBTTagCompound;
import net.minecraft.server.v1_8_R3.NBTTagList;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;

/**
 * @author BeastCube
 */
@Data
public class Skull {

    @SerializedName("name")
    private String name;

    @SerializedName("skullowner")
    private UUID skullOwner;

    @SerializedName("value")
    private String value;

    public ItemStack createSkull() {
        net.minecraft.server.v1_8_R3.ItemStack nmsStack = new net.minecraft.server.v1_8_R3.ItemStack(Items.SKULL, 1, 3);
        nmsStack.setTag(new NBTTagCompound());
        NBTTagCompound displayTag = new NBTTagCompound();
        NBTTagCompound entryTag = new NBTTagCompound();
        NBTTagCompound propertiesTag = new NBTTagCompound();
        NBTTagCompound skullOwnerTag = new NBTTagCompound();

        NBTTagList texturesList = new NBTTagList();

        displayTag.setString("Name", name);

        entryTag.setString("Value", value);

        texturesList.add(entryTag);

        propertiesTag.set("textures", texturesList);

        skullOwnerTag.setString("Id", skullOwner.toString());
        skullOwnerTag.set("Properties", propertiesTag);

        nmsStack.getTag().set("display", displayTag);
        nmsStack.getTag().set("SkullOwner", skullOwnerTag);
        return CraftItemStack.asBukkitCopy(nmsStack);
    }

}
