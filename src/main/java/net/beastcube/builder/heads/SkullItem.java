package net.beastcube.builder.heads;

import net.beastcube.api.bukkit.menus.events.ItemClickEvent;
import net.beastcube.api.bukkit.menus.items.MenuItem;
import net.beastcube.api.bukkit.menus.menus.ItemMenu;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * @author BeastCube
 */
public class SkullItem extends MenuItem {


    private final ItemStack stack;

    public SkullItem(ItemStack stack) {
        super(stack.getItemMeta().getDisplayName(), stack, "");
        this.stack = stack;
    }

    @Override
    public void onItemClick(ItemClickEvent event) {
        event.getPlayer().getInventory().addItem(stack);
    }

    @Override
    public ItemStack getFinalIcon(Player player, ItemMenu menu) {
        return stack;
    }
}
