package net.beastcube.builder.heads;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import net.beastcube.api.bukkit.commands.Command;
import net.beastcube.api.bukkit.menus.menus.ItemMenu;
import net.beastcube.api.commons.permissions.Permissions;
import net.beastcube.api.commons.text.ChatColor;
import net.beastcube.builder.Builder;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

/**
 * @author BeastCube
 */
public class Skulls {

    private Map<String, List<ItemStack>> skulls = new HashMap<>();

    @Command(identifiers = "skulls", permissions = Permissions.BUILDER)
    public void showSkullsMenu(Player sender) {
        ItemMenu menu = new ItemMenu(ChatColor.YELLOW + "Skulls", ItemMenu.Size.fit(CategoryItem.values().length), Builder.getInstance());
        for (int i = 0; i < CategoryItem.values().length; i++) {
            String categoryName = CategoryItem.values()[i].getName();
            menu.setItem(i, new SkullCategoryItem(categoryName, skulls.get(categoryName), menu));
        }
        menu.open(sender);
    }

    @Command(identifiers = "refreshskulls", permissions = Permissions.BUILDER)
    public void updateSkullsCommand(Player sender) {
        updateSkulls();
    }


    public void updateSkulls() {
        Bukkit.getScheduler().runTaskAsynchronously(Builder.getInstance(), () -> {
            for (CategoryItem category : CategoryItem.values()) {
                Builder.getInstance().getLogger().log(Level.INFO, "Updating Skulls for category " + category.getName());
                JsonArray json = SkullUtils.fromUrl(category.getUrl());
                addSkulls(category.getName(), json);
            }
        });
    }

    public void addSkulls(String categoryName, JsonArray json) {
        Gson gson = new Gson();
        List<Skull> list = gson.fromJson(json, new TypeToken<List<Skull>>() {
        }.getType());
        skulls.put(categoryName, new ArrayList<>());
        list.forEach(s -> skulls.get(categoryName).add(s.createSkull()));
    }

}
