package net.beastcube.builder;

import net.beastcube.api.commons.permissions.Permissions;
import net.beastcube.api.commons.permissions.Roles;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.hanging.HangingBreakEvent;
import org.bukkit.event.player.*;

import java.util.Optional;

/**
 * @author BeastCube
 */
public class BuilderEventListener implements Listener {

    Builder plugin;

    public BuilderEventListener(Builder plugin) {
        this.plugin = plugin;
    }

    private boolean setDirty(Player player) {
        if (player.getWorld().getName().equals("lobby") || plugin.isOnlySpectating(player)) {
            return true;
        }
        Optional<BuilderMap> map = plugin.getMapHandler().getBuilderMap(player.getWorld());
        if (map.isPresent()) {
            map.get().setDirty(true);
        }
        return false;
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        e.setCancelled(setDirty(e.getPlayer()));
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e) {
        e.setCancelled(setDirty(e.getPlayer()));
    }

    @EventHandler
    public void onHangingBreakByEntity(HangingBreakByEntityEvent e) {
        if (e.getEntity().getWorld().getName().equals("lobby"))
            e.setCancelled(true);
        if (e.getRemover() instanceof Player) {
            if (plugin.isOnlySpectating((Player) e.getRemover()))
                e.setCancelled(true);
        }
        Optional<BuilderMap> map = plugin.getMapHandler().getBuilderMap(e.getEntity().getWorld());
        if (map.isPresent())
            map.get().setDirty(true);
    }

    @EventHandler
    public void onHangingBreak(HangingBreakEvent e) {
        if (e.getEntity().getWorld().getName().equals("lobby"))
            e.setCancelled(true);
        Optional<BuilderMap> map = plugin.getMapHandler().getBuilderMap(e.getEntity().getWorld());
        if (map.isPresent())
            map.get().setDirty(true);
    }

    //TODO: Add all sorts of events that should mark the map as dirty

    @EventHandler
    public void onPlayerDropItem(PlayerDropItemEvent e) {
        e.setCancelled(setDirty(e.getPlayer()));
    }

    //TODO: unregister all player specific commands on player quit
    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        plugin.sendPlayerToLobby(e.getPlayer());
    }

    @EventHandler
    public void onPlayerChangedWorld(PlayerChangedWorldEvent e) {
        Optional<BuilderMap> map = Builder.getInstance().getMapHandler().getBuilderMap(e.getFrom());
        if (map.isPresent()) {
            if (e.getFrom().getPlayers().size() == 0) {
                Builder.getInstance().saveAndClose(null, map.get().getWorld(), map.get().getMinigame());
            }
        }
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        e.getPlayer().teleport(plugin.getLobbySpawn());
    }

    @EventHandler
    public void onPlayerRespawn(PlayerRespawnEvent e) {
        if (plugin.getRespawnLocation(e.getPlayer()) != null)
            e.setRespawnLocation(plugin.getRespawnLocation(e.getPlayer()));
    }

    @EventHandler
    public void onPlayerLoginEvent(PlayerLoginEvent e) {
        if (!Roles.getRole(e.getPlayer().getUniqueId()).hasPermission(Permissions.BUILDER)) {
            e.disallow(PlayerLoginEvent.Result.KICK_OTHER, "Du bist kein Architekt");
        }
    }

    @EventHandler
    public void onPlayerTeleport(PlayerTeleportEvent e) {
        if (e.getFrom().getWorld() != e.getTo().getWorld()) {
            Optional<BuilderMap> map = Builder.getInstance().getMapHandler().getBuilderMap(e.getTo().getWorld());
            if (map.isPresent()) {
                Builder.getInstance().selectMinigame(e.getPlayer(), map.get().getMinigame());
                Builder.getInstance().applyMapEffects(e.getPlayer(), map.get().getMinigame(), map.get().getName());
            }
        }
    }

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent e) {
        e.setKeepInventory(true);
    }

}
