package net.beastcube.builder;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * @author BeastCube
 *         Handles directory operations
 */
public final class DirectoryHandler {

    private static void initDirectory(String dir, Builder plugin) {
        File f = new File(plugin.getDataFolder(), "/" + dir + "/");
        if (!f.exists()) {
            Builder.getInstance().getLogger().info("Initialisiere " + f.getAbsolutePath());
            f.mkdirs();
        }
    }

    /**
     * @param src
     * @param dest
     * @throws IOException
     */
    public static void copyFolder(File src, File dest) throws IOException {
        if (src.isDirectory()) {

            //if directory not exists, create it
            if (!dest.exists()) {
                if (!dest.mkdirs()) {
                    Builder.getInstance().getLogger().info("Failed to create directory " + dest);
                } else {
                    Builder.getInstance().getLogger().info("Directory copied from " + src + "  to " + dest);
                }
            }
            //list all the directory contents
            String files[] = src.list();

            for (String file : files) {
                //construct the src and dest file structure
                File srcFile = new File(src, file);
                File destFile = new File(dest, file);
                //recursive copy
                copyFolder(srcFile, destFile);
            }

        } else {
            //if file, then copy it
            //Use bytes stream to support all file types
            InputStream in = new FileInputStream(src);
            OutputStream out = new FileOutputStream(dest);

            byte[] buffer = new byte[1024];

            int length;
            //copy the file content in bytes
            while ((length = in.read(buffer)) > 0) {
                out.write(buffer, 0, length);
            }

            in.close();
            out.close();
            Builder.getInstance().getLogger().info("File copied from " + src + " to " + dest);
        }
    }

    public static void deleteFolder(File folder) {
        if (folder.isDirectory()) {
            for (File f : folder.listFiles()) {
                deleteFolder(f);
            }
            try {
                Files.delete(Paths.get(folder.getAbsolutePath()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                Files.delete(Paths.get(folder.getAbsolutePath()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void init(Builder plugin) {
        initDirectory("minigames", plugin);
        initDirectory("worlds", plugin);
    }

    public static void initMinigameDirs(Builder plugin) {
        plugin.getMinigames().stream().forEach(type -> initDirectory("worlds/" + type, plugin));
    }

}
