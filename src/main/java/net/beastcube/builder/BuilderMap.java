package net.beastcube.builder;

import net.beastcube.api.bukkit.util.serialization.Serializer;
import net.beastcube.api.commons.minigames.MinigameType;
import net.beastcube.minigameapi.arena.Arena;
import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.World;

import java.io.File;
import java.io.IOException;

/**
 * @author BeastCube
 */
public class BuilderMap {
    private String name;
    private World world;
    private Arena arena;
    private MinigameType minigame;
    private boolean dirty;

    public BuilderMap(String name, World world, Arena arena, MinigameType minigame) {
        this.name = name;
        this.world = world;
        this.arena = arena;
        this.minigame = minigame;
        this.dirty = false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public World getWorld() {
        return world;
    }

    public void setWorld(World world) {
        this.world = world;
    }

    public Arena getArena() {
        return arena;
    }

    public void setArena(Arena arena) {
        this.arena = arena;
    }

    public void saveArena() throws IOException {
        File file = new File(new File(Bukkit.getWorldContainer(), this.getName()), "arena.json");
        FileUtils.writeStringToFile(file, Serializer.formatPretty(Serializer.serialize(getArena())), "UTF-8");
    }

    public MinigameType getMinigame() {
        return minigame;
    }

    public void setMinigame(MinigameType minigame) {
        this.minigame = minigame;
    }

    public void setDirty(boolean dirty) {
        this.dirty = dirty;
    }

    public boolean isDirty() {
        return this.dirty;
    }
}
