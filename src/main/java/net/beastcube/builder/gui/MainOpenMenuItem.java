package net.beastcube.builder.gui;

import net.beastcube.api.bukkit.menus.events.ItemClickEvent;
import net.beastcube.api.bukkit.menus.items.MenuItem;
import net.beastcube.api.bukkit.menus.menus.ItemMenu;
import net.beastcube.api.bukkit.menus.menus.PagedMenu;
import net.beastcube.api.commons.minigames.MinigameType;
import net.beastcube.builder.Builder;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author BeastCube
 */
public class MainOpenMenuItem extends MenuItem {

    private final ItemMenu parent;
    private static Map<MinigameType, Material> minigameTypeItems = new HashMap<>();

    static {
        minigameTypeItems.put(MinigameType.BEDWARS, Material.BED);
        minigameTypeItems.put(MinigameType.FFA, Material.IRON_SWORD);
        minigameTypeItems.put(MinigameType.SURVIVALGAMES, Material.CHEST);
        minigameTypeItems.put(MinigameType.BEASTRUSH, Material.MONSTER_EGG);
    }

    public MainOpenMenuItem(String displayName, ItemStack icon, ItemMenu parent, String... lore) {
        super(displayName, icon, lore);
        this.parent = parent;
    }

    @Override
    public void onItemClick(ItemClickEvent event) {

        ItemMenu menu = new PagedMenu("Map öffnen", ItemMenu.Size.fit(Builder.getInstance().getMinigames().size()), Builder.getInstance(), parent);
        Iterator<MinigameType> it = Builder.getInstance().getMinigames().iterator();
        int i = 0;
        while (it.hasNext()) {
            MinigameType type = it.next();
            Material material = minigameTypeItems.containsKey(type) ? minigameTypeItems.get(type) : Material.STONE;
            menu.setItem(i, new OpenMapsMenuItem(ChatColor.YELLOW + type.getDisplayName(), new ItemStack(material), menu, type));
            i++;
        }
        menu.open(event.getPlayer());
    }
}
