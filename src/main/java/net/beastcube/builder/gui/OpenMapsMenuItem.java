package net.beastcube.builder.gui;

import net.beastcube.api.bukkit.menus.events.ItemClickEvent;
import net.beastcube.api.bukkit.menus.items.MenuItem;
import net.beastcube.api.bukkit.menus.menus.ItemMenu;
import net.beastcube.api.bukkit.menus.menus.PagedMenu;
import net.beastcube.api.commons.minigames.MinigameType;
import net.beastcube.builder.Builder;
import net.beastcube.builder.MapHandler;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Optional;

/**
 * @author BeastCube
 */
public class OpenMapsMenuItem extends MenuItem {

    private final ItemMenu parent;
    private final MinigameType type;

    public OpenMapsMenuItem(String displayName, ItemStack icon, ItemMenu parent, MinigameType type, String... lore) {
        super(displayName, icon, lore);
        this.parent = parent;
        this.type = type;
    }

    @Override
    public void onItemClick(ItemClickEvent event) {
        Player p = event.getPlayer();
        MapHandler mapHandler = Builder.getInstance().getMapHandler();
        Optional<String[]> maps = mapHandler.getAllMaps(p, type);

        ItemMenu menu = new PagedMenu("Map öffnen", ItemMenu.Size.SIX_LINE, Builder.getInstance(), parent);

        if (maps.isPresent() && maps.get().length > 0) {
            for (int i = 0; i < maps.get().length; i++) {
                String mapName = maps.get()[i];

                //TODO menu.setItem(i, new )
                //in the map item: Builder.getInstance().selectAndLoad(p, type.name(), mapName);
            }
        } else {
            p.sendMessage("    " + ChatColor.RED + "Keine Maps vorhanden!");
        }
        menu.open(p);
    }
}
