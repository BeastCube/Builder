package net.beastcube.builder.gui;

import net.beastcube.api.bukkit.commands.Command;
import net.beastcube.api.bukkit.menus.menus.ItemMenu;
import net.beastcube.api.commons.permissions.Permissions;
import net.beastcube.builder.Builder;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * @author BeastCube
 */
public class GUI {

    @Command(identifiers = "menu", description = "Öffnet das Builder Menü", permissions = Permissions.BUILDER)
    public void showMenu(Player p) {
        ItemMenu menu = new ItemMenu(ChatColor.AQUA + "Builder Menu", ItemMenu.Size.ONE_LINE, Builder.getInstance());

        menu.setItem(0, new MainOpenMenuItem("Map öffnen", new ItemStack(Material.CHEST), menu));
        menu.open(p);
    }

}
