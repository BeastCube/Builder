package net.beastcube.builder;

import lombok.Getter;
import mkremins.fanciful.FancyMessage;
import net.beastcube.api.bukkit.BeastCubeAPI;
import net.beastcube.api.bukkit.commands.Arg;
import net.beastcube.api.bukkit.commands.Command;
import net.beastcube.api.bukkit.commands.CommandHandler;
import net.beastcube.api.commons.minigames.MinigameType;
import net.beastcube.api.commons.permissions.Permissions;
import net.beastcube.builder.gui.GUI;
import net.beastcube.builder.heads.Skulls;
import net.beastcube.minigameapi.Minigame;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

/**
 * @author BeastCube
 */
public class Builder extends JavaPlugin {

    private CommandHandler commandHandler;
    private MapHandler mapHandler;
    private HashMap<UUID, MinigameType> selectedMinigame = new HashMap<>();
    private HashMap<MinigameType, Class<? extends JavaPlugin>> minigames = new HashMap<>();

    @Getter
    BuilderConfig builderConfig;

    @Getter
    private static Builder instance;

    HashMap<UUID, Location> respawnLocations = new HashMap<>();
    HashMap<UUID, PermissionAttachment> permissionAttachments = new HashMap<>();
    Collection<UUID> onlySpectating = new ArrayList<>();

    private String getExtension(File file) {
        return file.getPath().substring(file.getPath().lastIndexOf('.') + 1);
    }

    private List<Class> getClassesInJar(File file) throws IOException, ClassNotFoundException {
        List<Class> classes = new ArrayList<>();

        JarInputStream jarstream = new JarInputStream(new FileInputStream(file));
        URLClassLoader loader = new URLClassLoader(new URL[]{file.toURL()}, this.getClass().getClassLoader());
        JarEntry entry;


        while (true) {
            entry = jarstream.getNextJarEntry();
            if (entry == null) {
                break;
            }
            if ((entry.getName().endsWith(".class"))) {
                String className = entry.getName().replaceAll("/", "\\.");
                String myClass = className.substring(0, className.lastIndexOf('.'));
                Class tClass = Class.forName(myClass, true, loader);
                classes.add(tClass);
            }
        }

        return classes;
    }

    private void loadMinigames() {
        File dir = new File(getDataFolder(), "/minigames/");
        if (!dir.exists())
            return;
        File[] files = dir.listFiles();
        if (files != null) {
            for (File f : files) {
                if (getExtension(f).equals("jar")) {
                    List<Class> classes;
                    try {
                        classes = getClassesInJar(f);
                        classes.stream().filter(clazz -> clazz.getSuperclass() == JavaPlugin.class && clazz.isAnnotationPresent(Minigame.class)).forEach(clazz -> {
                            getLogger().info("Adding " + clazz.getSimpleName() + " to the minigames");
                            minigames.put(((Minigame) clazz.getAnnotation(Minigame.class)).minigameType(), clazz);
                        });
                    } catch (IOException e) {
                        getLogger().info("Die Jar " + f.getPath() + " konnte nicht ordnungsgemäß geladen werden. IOException");
                        e.printStackTrace();
                    } catch (ClassNotFoundException e) {
                        getLogger().info("Die Jar " + f.getPath() + " konnte nicht ordnungsgemäß geladen werden. ClassNotFoundException");
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    public void onEnable() {
        instance = this;
        DirectoryHandler.init(this);
        loadMinigames();

        commandHandler = new CommandHandler(this);
        commandHandler.setPermissionHandler(BeastCubeAPI::handlePermission);
        commandHandler.registerCommands(this);
        commandHandler.registerCommands(new GUI());
        Skulls skulls = new Skulls();
        skulls.updateSkulls();
        commandHandler.registerCommands(skulls);

        mapHandler = new MapHandler(this);

        DirectoryHandler.initMinigameDirs(this);

        Bukkit.getPluginManager().registerEvents(new BuilderEventListener(this), this);

        builderConfig = new BuilderConfig(this);
    }

    @Override
    public void onDisable() {

    }

    public Optional<MinigameType> getMinigame(String name) {
        try {
            MinigameType type = MinigameType.valueOf(name.toUpperCase());
            if (minigames.keySet().contains(type)) {
                return Optional.of(type);
            }
        } catch (Exception ignored) {
        }
        return Optional.empty();
    }

    public Optional<Class<? extends JavaPlugin>> getMinigame(MinigameType type) {
        return Optional.ofNullable(minigames.get(type));
    }

    public Set<MinigameType> getMinigameNames() {
        return minigames.keySet();
    }

    public Set<MinigameType> getMinigames() {
        return minigames.keySet();
    }

    public void selectMinigame(Player p, MinigameType minigame) {
        selectedMinigame.put(p.getUniqueId(), minigame);
    }

    public boolean selectMinigame(Player p, String minigame) {
        Optional<MinigameType> type = getMinigame(minigame);
        if (type.isPresent()) {
            selectedMinigame.put(p.getUniqueId(), type.get());
            return true;
        } else {
            return false;
        }
    }

    public void deselectMinigame(Player p) {
        selectedMinigame.remove(p.getUniqueId());
    }

    public Optional<MinigameType> getSelectedMinigame(Player p) {
        return Optional.ofNullable(selectedMinigame.get(p.getUniqueId()));
    }

    public MapHandler getMapHandler() {
        return mapHandler;
    }

    public Location getLobbySpawn() {
        World lobby = Bukkit.getWorld("lobby");
        return lobby == null ? null : lobby.getSpawnLocation();
    }

    public Location getRespawnLocation(Player p) {
        return respawnLocations.get(p.getUniqueId());
    }

    public void setRespawnLocation(Player p, Location loc) {
        respawnLocations.put(p.getUniqueId(), loc);
    }

    public boolean isOnlySpectating(Player p) {
        return onlySpectating.contains(p.getUniqueId());
    }

    public void applyMapEffects(Player p, MinigameType minigame, String name) {
        Optional<BuilderMap> current = mapHandler.getBuilderMap(p.getWorld());
        if (current.isPresent()) {
            if (current.get().getArena() != null) {
                commandHandler.unregisterCommands(current.get().getArena(), p);
            }
        }

        registerPermissions(p);

        onlySpectating.remove(p.getUniqueId());

        p.setGameMode(GameMode.CREATIVE);

        setRespawnLocation(p, mapHandler.getSpawn(minigame, name));

        Optional<BuilderMap> map = mapHandler.getBuilderMap(minigame, name);
        if (map.isPresent()) {
            commandHandler.registerCommands(map.get().getArena(), p);
        }
    }

    public PermissionAttachment getPermissionAttachment(Player p) {
        PermissionAttachment attachment = permissionAttachments.get(p.getUniqueId());
        if (attachment == null) {
            attachment = p.addAttachment(this);
            permissionAttachments.put(p.getUniqueId(), attachment);
        }
        return attachment;
    }

    public void registerPermissions(Player p) {
        getPermissionAttachment(p).setPermission("worldedit.*", true); //TODO config
        getPermissionAttachment(p).setPermission("astools.*", true);
    }

    public void unregisterPermissions(Player p) {
        getPermissionAttachment(p).unsetPermission("worldedit.*");
        getPermissionAttachment(p).unsetPermission("astools.*");
    }


    public void sendPlayerToMap(Player p, MinigameType minigame, String name) {
        if (mapHandler.getBuilderMap(minigame, name).isPresent()) {
            p.teleport(mapHandler.getSpawn(minigame, name));
            applyMapEffects(p, minigame, name);
        }
    }

    public void sendPlayerToMapSpectating(Player p, MinigameType minigame, String name) {
        Optional<BuilderMap> current = mapHandler.getBuilderMap(p.getWorld());
        if (current.isPresent()) {
            if (current.get().getArena() != null) {
                commandHandler.unregisterCommands(current.get().getArena(), p);
            }
        }

        unregisterPermissions(p);

        p.teleport(mapHandler.getSpawn(minigame, name));

        p.setGameMode(GameMode.CREATIVE);

        setRespawnLocation(p, mapHandler.getSpawn(minigame, name));

        onlySpectating.add(p.getUniqueId());
    }

    @Command(identifiers = "builderlobby", description = "Teleportiert dich in die Lobby", permissions = Permissions.BUILDER)
    public void sendPlayerToLobby(Player p) {
        PermissionAttachment attachment = permissionAttachments.get(p.getUniqueId());
        if (attachment == null) {
            attachment = p.addAttachment(this);
            permissionAttachments.put(p.getUniqueId(), attachment);
        }
        unregisterPermissions(p);

        p.setGameMode(GameMode.SURVIVAL);

        onlySpectating.remove(p.getUniqueId());

        p.teleport(getLobbySpawn());

        setRespawnLocation(p, getLobbySpawn());

        Optional<BuilderMap> current = mapHandler.getBuilderMap(p.getWorld());
        if (current.isPresent()) {
            if (current.get().getArena() != null) {
                commandHandler.unregisterCommands(current.get().getArena(), p);
            }
        }
    }

    @Command(identifiers = "selectminigame", description = "Wählt ein Minigame aus, um für dieses eine Map zu setzen.")
    public void selectMinigameByCommand(Player p, @Arg(name = "Minigame") String minigame) {
        Optional<MinigameType> minigameType = getMinigame(minigame);
        if (minigameType.isPresent()) {
            selectMinigame(p, minigameType.get());
            p.sendMessage(ChatColor.GREEN + "Du baust nun für " + minigameType.get().getDisplayName());
        } else {
            p.sendMessage(ChatColor.RED + "Dieses Minigame existiert nicht. Hast du dich vertippt?");
            listMinigames(p);
        }
    }

    @Command(identifiers = "deselectminigame", description = "Wählt kein Minigame mehr aus.")
    public void deselectMinigameByCommand(Player p) {
        deselectMinigame(p);
        p.sendMessage(ChatColor.GREEN + "Du baust nun für kein Minigame mehr.");
    }

    @Command(identifiers = "listminigames", description = "Listet alle Minigames, für die du bauen kannst, auf.", permissions = Permissions.BUILDER /*ranks = {StaffRank.BESITZER, StaffRank.ADMINISTRATOR, StaffRank.MODERATOR, StaffRank.HELFER, StaffRank.ARCHITEKT, StaffRank.BAUARBEITER}*/)
    public void listMinigames(Player p) {
        p.sendMessage(ChatColor.GREEN + "Minigames:");
        for (MinigameType minigame : getMinigameNames()) {
            FancyMessage message = new FancyMessage(ChatColor.GOLD + minigame.getDisplayName());
            message.then(ChatColor.AQUA + "[" + ChatColor.GOLD + "Select" + ChatColor.AQUA + "]").command("/selectminigame " + minigame.name());
            message.send(p);
        }
    }

    @Command(identifiers = "create", description = "Erstellt eine neue Map, die du bauen kannst", permissions = Permissions.BUILDER /*ranks = {StaffRank.BESITZER, StaffRank.ADMINISTRATOR, StaffRank.MODERATOR, StaffRank.ARCHITEKT, StaffRank.BAUARBEITER}*/)
    public void buildNew(Player p, @Arg(name = "Name", description = "Der Name für die neue Map", verifiers = "exclude[lobby]") String name) {
        if (isMinigameSelected(p)) {
            Optional<MinigameType> minigame = getSelectedMinigame(p);

            if (mapHandler.doesMapExist(minigame.get(), name)) {
                p.sendMessage(ChatColor.RED + "Es existiert bereits eine Map mit diesem Namen!");
                return;
            }

            mapHandler.createNewMap(p, minigame.get(), name);

            mapHandler.saveMap(p, minigame.get(), name);

            sendPlayerToMap(p, minigame.get(), name);
        }
    }

    @Command(identifiers = "open", description = "Öffnet eine Map um weiterzubauen", permissions = Permissions.BUILDER /*ranks = {StaffRank.BESITZER, StaffRank.ADMINISTRATOR, StaffRank.MODERATOR, StaffRank.ARCHITEKT, StaffRank.BAUARBEITER}*/)
    public void open(Player p, @Arg(name = "Name", description = "Der Name der Map", verifiers = "exclude[lobby]") String name) {
        if (isMinigameSelected(p)) {
            Optional<MinigameType> minigame = getSelectedMinigame(p);

            if (!mapHandler.getBuilderMap(minigame.get(), name).isPresent()) {
                mapHandler.openMap(p, name, selectedMinigame.get(p.getUniqueId()));
            }

            sendPlayerToMap(p, minigame.get(), name);
        }
    }

    @Command(identifiers = "save", description = "Speichert die aktuelle Map", permissions = Permissions.BUILDER /*ranks = {StaffRank.BESITZER, StaffRank.ADMINISTRATOR, StaffRank.MODERATOR, StaffRank.HELFER, StaffRank.ARCHITEKT, StaffRank.BAUARBEITER}*/)
    public void save(Player p) {
        if (isMinigameSelected(p)) {
            save(p, p.getWorld(), getSelectedMinigame(p).get());
        }

    }

    public void save(Player p, World world, MinigameType minigameType) {
        mapHandler.saveMap(p, minigameType, world.getName());
    }

    @Command(identifiers = "watch", description = "Teleportiert dich zu der angegebenen Welt um sie anschauen zu können. Du kannst dort nichts bauen.")
    public void watch(Player p, @Arg(name = "map") String name) { //TODO Gamemode 3?
        if (isMinigameSelected(p)) {
            Optional<MinigameType> minigame = getSelectedMinigame(p);
            if (!mapHandler.doesMapExist(minigame.get(), name)) {
                p.sendMessage(ChatColor.RED + "Diese Map existiert nicht!");
                return;
            }

            if (!mapHandler.getBuilderMap(minigame.get(), name).isPresent()) {
                p.sendMessage(ChatColor.RED + "Die Map wurde nicht geöffnet!");
                return;
            }

            sendPlayerToMapSpectating(p, minigame.get(), name);
        }
    }

    @Command(identifiers = "kickfrommap", description = "Entfernt einen Spieler von der aktuellen Map, sodass er dort nicht mehr mitbauen darf", permissions = Permissions.BUILDER /*ranks = {StaffRank.BESITZER, StaffRank.ADMINISTRATOR, StaffRank.MODERATOR, StaffRank.ARCHITEKT, StaffRank.BAUARBEITER}*/)
    public void kickFromMap(Player p, @Arg(name = "player") Player tokick) {
        if (isMinigameSelected(p)) {
            Optional<BuilderMap> map = mapHandler.getBuilderMap(p.getWorld());
            if (!map.isPresent()) {
                p.sendMessage(ChatColor.RED + "Du musst zuerst eine Map öffnen!");
                return;
            }

            sendPlayerToLobby(tokick);
            tokick.sendMessage(ChatColor.RED + "Du wurdest von " + p.getName() + " von der Map " + map.get().getName() + " gekickt.");
        }
    }

    @Command(identifiers = "close", description = "Schliesst die aktuelle Map und teleportiert dich zurück in die Lobby", permissions = Permissions.BUILDER /*ranks = {StaffRank.BESITZER, StaffRank.ADMINISTRATOR, StaffRank.MODERATOR, StaffRank.ARCHITEKT, StaffRank.BAUARBEITER}*/)
    public void close(Player p) {
        if (isMinigameSelected(p)) {
            Optional<MinigameType> minigame = getSelectedMinigame(p);
            String name = p.getWorld().getName();

            Optional<BuilderMap> map = mapHandler.getBuilderMap(minigame.get(), name);
            if (!map.isPresent()) {
                p.sendMessage(ChatColor.RED + "Die Map wurde nicht geöffnet!");
                return;
            }

            if (map.get().isDirty()) {
                p.sendMessage("");
                p.sendMessage("");
                p.sendMessage(ChatColor.RED + "Die Map wurde nicht gespeichert.");
                p.sendMessage(ChatColor.RED + "Ungespeicherte Änderungen gehen ohne Speichern verloren.");
                p.sendMessage(ChatColor.RED + "Möchtest du sie speichern?");
                FancyMessage message = new FancyMessage("  ");
                message.then(ChatColor.AQUA + "[" + ChatColor.GREEN + "Ja" + ChatColor.AQUA + "]").command("/saveandclose");
                message.then("  ");
                message.then(ChatColor.AQUA + "[" + ChatColor.RED + "Nein" + ChatColor.AQUA + "]").command("/finalclose");
                message.send(p);
            } else {
                finalClose(p);
            }
        }
    }

    @Command(identifiers = "saveandclose", description = "Speichert die aktuelle Map und schließt sie dann.", permissions = Permissions.BUILDER /*ranks = {StaffRank.BESITZER, StaffRank.ADMINISTRATOR, StaffRank.MODERATOR, StaffRank.ARCHITEKT, StaffRank.BAUARBEITER}*/)
    public void saveAndClose(Player p) {
        save(p);
        finalClose(p);
    }

    public void saveAndClose(Player p, World world, MinigameType minigameType) {
        save(p, world, minigameType);
        finalClose(p, world, minigameType);
    }

    @Command(identifiers = "finalclose", description = "Schließt die aktuelle Map ohne Nachfrage ob sie gespeichert werden soll.", permissions = Permissions.BUILDER /*ranks = {StaffRank.BESITZER, StaffRank.ADMINISTRATOR, StaffRank.MODERATOR, StaffRank.ARCHITEKT, StaffRank.BAUARBEITER}*/)
    public void finalClose(Player p) {
        if (isMinigameSelected(p)) {
            Optional<MinigameType> minigame = getSelectedMinigame(p);
            sendPlayerToLobby(p);
            finalClose(p, p.getWorld(), minigame.get());
        }
    }

    public void finalClose(Player p, World world, MinigameType minigameType) {
        mapHandler.closeMap(p, minigameType, world.getName());
    }

    private boolean isMinigameSelected(Player p) {
        Optional<MinigameType> selectedMinigame = getSelectedMinigame(p);
        if (!selectedMinigame.isPresent()) {
            p.sendMessage(ChatColor.RED + "Du musst zuerst ein Minigame auswählen. /selectminigame");
            return false;
        }
        return true;
    }

    @Command(identifiers = "export", description = "Exportiert eine Map um sie später spielen zu können", permissions = Permissions.BUILDER_SPECIAL /*ranks = {StaffRank.BESITZER, StaffRank.ADMINISTRATOR}*/)
    public void export(Player p) {
        if (isMinigameSelected(p)) {
            Optional<MinigameType> minigame = getSelectedMinigame(p);
            String name = p.getWorld().getName();

            mapHandler.exportMap(p, minigame.get(), name);
        }
    }

    @Command(identifiers = "delete", description = "Löscht eine Map", permissions = Permissions.BUILDER_SPECIAL /*ranks = {StaffRank.BESITZER, StaffRank.ADMINISTRATOR, StaffRank.MODERATOR, StaffRank.ARCHITEKT, StaffRank.BAUARBEITER}*/)
    public void delete(Player p, @Arg(name = "minigameType") String minigameType, @Arg(name = "map") String map) {
        Optional<MinigameType> minigame = getMinigame(minigameType);

        if (!minigame.isPresent()) {
            p.sendMessage(ChatColor.RED + "Dieses Minigame existiert nicht!");
            return;
        }

        if (mapHandler.getBuilderMap(minigame.get(), map).isPresent()) {
            p.sendMessage(ChatColor.RED + "Die Map ist zurzeit geöffnet. Schliesse sie bevor du sie löschst.");
            return;
        }

        if (!mapHandler.doesMapExist(minigame.get(), map)) {
            p.sendMessage(ChatColor.RED + "Die Map existiert nicht.");
            return;
        }

        mapHandler.deleteMap(p, minigame.get(), map);
    }

    @Command(identifiers = "selectandload", description = "Wählt das Minigame aus und laded gleichzeitig die Map", permissions = Permissions.BUILDER /*ranks = {StaffRank.BESITZER, StaffRank.ADMINISTRATOR, StaffRank.MODERATOR, StaffRank.ARCHITEKT, StaffRank.BAUARBEITER}*/)
    public void selectAndLoad(Player p, @Arg(name = "minigame") String minigame, @Arg(name = "map") String map) {
        selectMinigameByCommand(p, minigame);
        open(p, map);
    }

    @Command(identifiers = "allmaps", description = "Listet alle Maps auf", permissions = Permissions.BUILDER /*ranks = {StaffRank.BESITZER, StaffRank.ADMINISTRATOR, StaffRank.ARCHITEKT}*/)
    public void listAllMaps(Player p) {
        p.sendMessage(ChatColor.GREEN + "Maps:");

        for (MinigameType mg : getMinigames()) {
            p.sendMessage(" " + ChatColor.GOLD + mg.getDisplayName() + ":");
            Optional<String[]> maps = mapHandler.getAllMaps(p, mg);
            if (maps.isPresent() && maps.get().length > 0) {
                for (String map : maps.get()) {
                    FancyMessage message = new FancyMessage("    ");
                    message.then(map).color(ChatColor.AQUA).command("/selectandload " + mg.name() + " " + map).tooltip("Laden");
                    message.send(p);
                }
            } else {
                p.sendMessage("    " + ChatColor.RED + "Keine Maps vorhanden!");
            }
        }
    }

    @Command(identifiers = "searchplayer", description = "Zeigt dir die Map an, auf der der Spieler gerade baut", permissions = Permissions.BUILDER /*ranks = {StaffRank.BESITZER, StaffRank.ADMINISTRATOR, StaffRank.MODERATOR}*/)
    public void searchPlayer(Player p, @Arg(name = "player") Player player) {
        if (!player.isOnline()) {
            p.sendMessage(ChatColor.RED + "Dieser Spieler ist nicht online.");
            return;
        }

        if (player.getWorld().getName().equals("lobby")) {
            p.sendMessage(ChatColor.GOLD + player.getName() + " befindet sich zurzeit auf der Lobby");
        } else {
            Optional<BuilderMap> map = mapHandler.getBuilderMap(player.getWorld());
            if (map.isPresent()) {
                p.sendMessage(ChatColor.GOLD + player.getName() + " befindet sich zurzeit auf der Map " + map.get().getName() + " für das Minigame " + map.get().getMinigame());
            } else {
                p.sendMessage(ChatColor.RED + "Oops. " + player.getName() + " befindet sich auf einer nicht geladenen Map!");
            }
        }
    }

    @Command(identifiers = "mapinfo", description = "Ruft Informationen über eine bestimmte Map ab", permissions = Permissions.BUILDER /*ranks = {StaffRank.BESITZER, StaffRank.ADMINISTRATOR}*/)
    public void mapInfo(Player sender, @Arg(name = "mapname") String mapname) {
        sender.sendMessage(ChatColor.GOLD + "MapInfo for map: " + ChatColor.DARK_PURPLE + mapname);
        //TODO
    }
}
